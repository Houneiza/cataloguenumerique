/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.services.impl;

import cm.maroua.univ.enspm.catalogue.dao.SousCategorieDao;
import cm.maroua.univ.enspm.catalogue.entities.SousCategorie;
import cm.maroua.univ.enspm.catalogue.services.ISousCategorieRessource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author hounie
 */
public class SousCategorieRessource implements ISousCategorieRessource {
    @Autowired
    private SousCategorieDao sousCategorieDao;
    
    @Override
    public Page<SousCategorie> getAllSousCategories(int page, int pagesize) {
          return sousCategorieDao.findAll(PageRequest.of(page, pagesize));     }
    
    @Override
    public Page<SousCategorie> searchSousCategories(String name, int page, int pagesize) {
        return sousCategorieDao.findAll(PageRequest.of(page, pagesize));
    }

    @Override
    public ResponseEntity<SousCategorie> findSousCategorie(long id) {
        Optional<SousCategorie> sousCategorie = sousCategorieDao.findById(id);
        if (sousCategorie.isPresent()) {
            return ResponseEntity.ok(sousCategorie.get());
        }
        return ResponseEntity.notFound().build();     }

    @Override
    public ResponseEntity<SousCategorie> updateSousCategorie(long id, SousCategorie souscategorie) {
          return sousCategorieDao.findById(id).map(
                c -> {
                    c.setName(souscategorie.getName());
                    return ResponseEntity.ok(sousCategorieDao.save(c));
                }
        ).orElse(
                ResponseEntity.notFound().build()
        );     }

    @Override
    public ResponseEntity<SousCategorie> addSousCategorie(SousCategorie souscategorie) {
        SousCategorie c = sousCategorieDao.save(souscategorie);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(c.getId())
                .toUri();
        return ResponseEntity.created(location).body(c);     }

    @Override
    public void deleteSousCategorie(long id) {
        sousCategorieDao.deleteById(id);    }

    }
    
  
