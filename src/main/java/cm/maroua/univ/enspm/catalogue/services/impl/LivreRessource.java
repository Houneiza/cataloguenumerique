/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.services.impl;

import cm.maroua.univ.enspm.catalogue.dao.LivreDao;
import cm.maroua.univ.enspm.catalogue.entities.Auteur;
import cm.maroua.univ.enspm.catalogue.entities.Livre;
import cm.maroua.univ.enspm.catalogue.entities.SousCategorie;
import cm.maroua.univ.enspm.catalogue.services.ILivreRessource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


/**
 *
 * @author hounie
 */

public class LivreRessource implements ILivreRessource{
     @Autowired
     private LivreDao livreDao; 

    @Override
    public Page<Livre> getAllLivres(int page, int pagesize) {
        return livreDao.findAll(PageRequest.of(page, pagesize));
    }

    @Override
    public Page<Livre> searchLivresByTitre(String titre, int page, int pagesize) {
        return livreDao.findByTitreLike("%" + titre + "%", PageRequest.of(page, pagesize));

    }

  @Override
    public Page<Livre> searchLivresByAuteurNomLike(String nom, int page, int pagesize) {
        return livreDao.findByAuteurNomLike("%" + nom+ "%", PageRequest.of(page, pagesize));
    }

    @Override
    public Page<Livre> searchLivresBySousCategorieNameLike(String name, int page, int pagesize) {
        return livreDao.findBySousCategorieNameLike("%" + name + "%", PageRequest.of(page, pagesize));
    }

    @Override
    public ResponseEntity<Livre> findLivre(long id) {
    Optional<Livre> livre = livreDao.findById(id);
        if (livre.isPresent()) {
            return ResponseEntity.ok(livre.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Livre> updateLivre(long id, Livre livre) {
         return livreDao.findById(id).map(
                c -> {
                    c.setTitre(livre.getTitre());
                    c.setDate_publication(livre.getDate_publication());
                    c.setAuteur(livre.getAuteur());
                    c.setSousCategorie(livre.getSousCategorie());
                    return ResponseEntity.ok(livreDao.save(c));
                }
        ).orElse(
                ResponseEntity.notFound().build()
        );    }
    

    @Override
    public ResponseEntity<Livre> addLivre(Livre livre) {
         Livre c = livreDao.save(livre);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(c.getId())
                .toUri();
        return ResponseEntity.created(location).body(c);    }

    
    @Override
    public void deleteLivre(long id) {
        livreDao.deleteById(id);    }
    
}
