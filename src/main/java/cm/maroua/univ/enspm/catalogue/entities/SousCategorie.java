
package cm.maroua.univ.enspm.catalogue.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;


@Entity
@Data
public class SousCategorie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @XmlTransient
     @OneToMany(mappedBy = "sousCategorie")
    private List<Livre> livres; 
      
    @ManyToOne
    private Categorie categorie;
     
}
