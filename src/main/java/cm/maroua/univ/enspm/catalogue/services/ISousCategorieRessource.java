
package cm.maroua.univ.enspm.catalogue.services;

import cm.maroua.univ.enspm.catalogue.entities.SousCategorie;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author hounie
 */
@Path("/souscategories")
public interface ISousCategorieRessource {
   
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<SousCategorie> getAllSousCategories(@DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    
    @GET
    @Path("/search")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<SousCategorie> searchSousCategories(@QueryParam("name")String name, @DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    
    @GET
    @Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<SousCategorie> findSousCategorie(@PathParam("id") long id);
    
    @PUT
    @Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<SousCategorie> updateSousCategorie(@PathParam("id") long id,SousCategorie souscategorie);
    
    @POST
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<SousCategorie> addSousCategorie(SousCategorie souscategorie);
    
    @DELETE
    @Path("{id: \\d+}")
    public void deleteSousCategorie(@PathParam("id") long id);
}
